import styled from "styled-components"
import Navigation from "../components/navigation"
import { useRouter } from "next/router";

const List = () => {
    return (
        <>
        <Navigation />
        <Main>
          <ContentList>
            <ListItem />
            <ListItem />
            <ListItem />
            <ListItem />
            <ListItem />
            <ListItem />
            <ListItem />
          </ContentList>
          <Sidebar />
        </Main>
        </>
    )
}

export default List;

const Main = styled.section `
  margin-top: 80px;
  padding: 30px;
  overflow-y: scroll;
  width: 100%;
  height: calc(100% - 80px);
  position: absolute;
  background-color: lightgrey;
  display: grid;
  grid-template-columns: auto 30%;
  grid-gap: 1em;
`
const ContentList = styled.div `
  display: grid;
  grid-template-columns: repeat(2, auto);
  grid-gap: 1em;
`
const ListItem = styled.div `
  height: 200px;
  background-color: green;
`
const Sidebar = styled.div `
  height: 100%;
  background-color: skyblue;
`