import styled, { keyframes } from "styled-components"
import { BiSearchAlt } from 'react-icons/bi'
import Footer from "../components/footer";
import { useState } from "react";
import Image from 'next/image'
import Homepage from "../components/Homepage";

export default function Home() {
  const [query, setQuery] = useState("");
  const handleChange = (event) => {
    setQuery(event.target.value);
  };

  const submitSearch = (event) => {
    event.preventDefault();
    setQuery(query);
    if(query.length > 3){
      window.location.href = `/search/${query}`
    }else{
      alert("Harap ketik minimal 4 karakter/huruf pada form pencarian")
    }
  };
  return (
    <>
      <Wraper>
        <Logo>
          <Image src="/logo.svg" alt="Dapur Ibu" width="300" height="250" />
        </Logo>
        <SearchForm onSubmit={submitSearch}>
          <InputText placeholder="Cari Resep" onChange={handleChange} />
          <BtnSearch type="submit">
            <BiSearchAlt />
          </BtnSearch>
        </SearchForm>
        <Main>
          <Homepage />
        </Main>
        <Footer />
      </Wraper>
    </>
  )
}

const Wraper = styled.div `
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow-y: auto;
  padding: 10px;

  & footer {
    text-align: center;
    color: gray;
  }
  
`
const Logo = styled.div `
  display: flex;
  justify-content: center;
  margin: 10px auto;
  width: 100%;

  & img {
    width: auto;
    height: 150px;
  }
`
const SearchForm = styled.form `
  display: flex;
  justify-content: center;
  width: 100%;
`
const InputText = styled.input.attrs({
  type: 'text'
})`
  width: 100%;
  max-width: 640px;
  padding: 10px;
  font-size: 16px;
  border: 3px solid orange;
  border-radius: 15px 0 0 15px;
  box-sizing: border-box;
  resize: vertical;

  &:focus {
      outline: none;
  }
`
const BtnSearch = styled.button `
  color: white;
  padding: 0 10px 0 5px;
  border-radius: 0 15px 15px 0;
  background-color: orange;
  border: none;
  font-size: 32px;
  font-weight: bold;
  display: flex;
  align-items: center;
  cursor: pointer;
`
const Main = styled.div `
  width: 100%;
  padding: 20px 50px;
  display: grid;
  grid-template-columns: repeat(3, auto);
  grid-gap: 1em;
  min-height: calc(100vh - 280px);

  @media(max-width: 960px){
    padding: 20px 20px;
  }
  @media(max-width: 760px){
    padding: 20px 10px;
    grid-template-columns: repeat(2, auto);
  }
  @media(max-width: 480px){
    padding: 20px 5px;
    grid-template-columns: auto;
  }
`
