import styled from "styled-components";
import Navigation from "../../components/navigation";
import loadable from "@loadable/component";
import Loader from "../../components/loader";
import Footer from "../../components/footer";

const Sidebar = loadable(() => import("../../components/sidebar"))
const DisplayList = loadable(() => import('../../components/displayList'), {
  fallback: <Loader />}, { ssr: true })

const List = (props) => {
  //console.log(props.data)  
  const thisData = props.data;

  return (
    <>
      <Navigation />
      <Wraper>  
      <Main>
          <DisplayList categoryName={thisData} />
          
        <Sidebar />
      </Main>
      <Footer />
      </Wraper>
    </>
  );
};

List.getInitialProps = async function (context) {
  const res = await fetch(
    `https://masak-apa-tomorisakura.vercel.app/api/categorys/recipes/${context.query.name}`
  );
  const data = await res.json();
  const dataList = Object.values(data)[2];
  
  return {
    data: dataList
  };
};

export default List;

const Wraper = styled.div `
  top: 80px;
  left:0;
  padding: 50px 100px 10px;
  overflow-y: auto;
  width: 100%;
  height: calc(100% - 80px);
  position: absolute;
  background-color: LightGoldenrodYellow;
  z-index: -5;

  & footer {
    text-align: center;
    color: gray;
  }
  @media(max-width: 960px){
    padding: 50px 30px 10px;
  }
  @media(max-width: 760px){
    padding: 50px 10px 10px;
  }
  @media(max-width: 480px){
    padding: 50px 5px 10px;
  }
`
const Main = styled.section`
  min-height: calc(100vh - 180px);
  display: grid;
  grid-template-columns: auto 30%;
  grid-gap: 1.5em;
  
  @media(max-width: 960px){
    grid-gap: 1em;
  }
  @media(max-width: 760px){
    grid-template-columns: auto;
  }
`;


