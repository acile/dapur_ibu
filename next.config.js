module.exports = {
  reactStrictMode: true,
}
// next.config.js
module.exports = {
  crossOrigin: 'anonymous',
}

const LoadablePlugin = require('@loadable/webpack-plugin')
module.exports = {
  // ...
  plugins: [new LoadablePlugin()],
}