import styled, {createGlobalStyle} from 'styled-components';

const GlobalStyles = createGlobalStyle`

*{
    margin: 0;
    padding: 0;
    box-sizing: inherit;
}

html {
    box-sizing: border-box;
    font-size: 90%;

    @media only screen and (min-width: 1980px){
        font-size: 120%;
    }
}
body{
    font-family: 'B612', sans-serif;
    font-weight: 400;
    line-height: 1.6;
    font-size: 1.6rem;
    background: teal;
    color: #333;
    overflow: hidden;
}
a:hover, a:active {
    text-shadow: 0px 6px 10px rgba(0, 0, 0, 0.5);
    color: teal;
}
`;

export default GlobalStyles;

// add it to the App.js file and put it at the top.
<GlobalStyles/>