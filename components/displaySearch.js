import styled from "styled-components";
import { MdRoomService } from "react-icons/md";
import { GiCookingPot } from "react-icons/gi";
import Link from 'next/link'

export default function DisplayList({queryName}) {
    const thisData = queryName;
    return (
        <>
        <ContentList>
        {thisData.map(({ title, key, thumb, serving, difficulty }, index) => (
            <a href={`/detail/${key}`} key={index}>
            <ListItem  img={thumb}>
              <Label>
                <LabelItem>
                <MdRoomService /> 
                <span>{serving}</span>
                </LabelItem>
                <LabelItem>
                <GiCookingPot />
                <span>{difficulty}</span>
                </LabelItem>
              </Label>
              <Title>{title}</Title>
            </ListItem>
            </a>
          ))}
          </ContentList>
          </>
    )
}

const Label = styled.div`
  background-color: gold;
  width: 250px;
  float: right;
  margin-top: -23px;
  margin-right: -3px;
  border-radius: 15px 15px 0 15px;
  padding: 5px 10px;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  border: 3px solid gold;
`;
const LabelItem = styled.div `
  display: flex;
  align-items: center;

  & svg {
    font-size: 24px;
    font-weight: bold;
    margin-right: 4px;
  }
`
const Title = styled.h1`
  display: flex;
  align-items: flex-end;
  height: 175px;
  width: 100%;
  padding: 0.5em;
  margin-bottom: 0;
  color: black;
`;
const ListItem = styled.div`
  background-color: rgba(240, 240, 240, 0.5);
  height: 200px;
  width: 100%;
  position: relative;
  cursor: pointer;
  border: 3px solid gold;
  border-radius: 17px 0 17px 17px;

  &:after {
    content: "";
    background-image: ${(props) => `url(${props.img})`};
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    position: absolute;
    z-index: -1;
    border-radius: 15px 0 15px 15px;
  }
`;
const ContentList = styled.div`
  display: grid;
  grid-template-columns: repeat(2, auto);
  grid-column-gap: 1em;
  grid-row-gap: 1.75em;
  margin-bottom: 20px;

  @media(max-width: 960px){
    grid-column-gap: .5em;
  }
  @media(max-width: 480px){
    grid-template-columns: auto;
  }
`;