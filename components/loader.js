import styled from "styled-components";

export default function Loader() {
    return (
        <>
        <Loading>Loading...</Loading>
        </>
    )
}

const Loading = styled.div `
  display: flex;
  width: 100%;
  height: 100%;
  justify-content: center;
  font-size: 2em;
  font-weight: bold;
  color: DarkKhaki;
`