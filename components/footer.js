import styled from "styled-components"

export default function Footer() {
    return (
        <Foo>ⓒ2021_ACILE</Foo>
    )
}

const Foo = styled.footer `
    text-align: center;
    color: gray;
`