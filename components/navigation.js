import styled from "styled-components";
import { BiSearchAlt, BiX, BiMenu } from "react-icons/bi";
import { useState, useRouter } from "react";
import Image from 'next/image'
import Link from 'next/link'

const category = [
  {
    id: 1,
    name: "resep-daging",
    title: "Olahan Daging",
  },
  {
    id: 2,
    name: "resep-sayuran",
    title: "Olahan Sayur",
  },
  {
    id: 3,
    name: "resep-ayam",
    title: "Olahan Ayam",
  },
  {
    id: 5,
    name: "resep-seafood",
    title: "Olahan Seafood",
  },
  {
    id: 4,
    name: "resep-dessert",
    title: "Aneka Dessert",
  },
  {
    id: 6,
    name: "masakan-tradisional",
    title: "Menu Tradisional",
  },
];

export default function Navigation() {
  const [query, setQuery] = useState("");
  const handleChange = (event) => {
    setQuery(event.target.value);
  };

  const submitSearch = (event) => {
    event.preventDefault();
    setQuery(query);
    if (query.length > 3) {
      window.location.href = `/search/${query}`;
    } else {
      alert("Harap ketik minimal 4 karakter/huruf pada form pencarian");
    }
  };

  const [click, setClick] = useState(false);
  const handleClick = () => setClick(!click);
  const closeMenu = () => setClick(false);
  //const router = useRouter();
  return (
    <>
      <Nav>
        <SearchForm onSubmit={submitSearch}>
          <InputText placeholder="Cari Resep" onChange={handleChange} />
          <BtnSearch type="submit">
            <BiSearchAlt />
          </BtnSearch>
        </SearchForm>

        <MenuIcon onClick={handleClick}>
          {click ? <BiX /> : <BiMenu />}
        </MenuIcon>
        <MainMenu click={click}>
          <Menu>
            <SearchFormMenu onSubmit={submitSearch}>
              <InputText placeholder="Cari Resep" onChange={handleChange} />
              <BtnSearch type="submit">
                <BiSearchAlt />
              </BtnSearch>
            </SearchFormMenu>
            {category.map((list) => (
              <MenuItem key={`category-${list.id}`}>
                <Button href={`/list/${list.name}`} >{list.title}</Button>
              </MenuItem>
            ))}
          </Menu>
        </MainMenu>
      </Nav>

      <Link href="/">
        <Logo>
          <Image src="/logo.svg" alt="Dapur Ibu" width="300" height="250" />
        </Logo>
      </Link>
    </>
  );
}

const MainMenu = styled.div`
  display: none;
  @media (max-width: 760px) {
    display: block;
    right: ${({ click }) => (click ? "0" : "-100%")};
    background-color: rgba(0, 0, 0, 0.9);
    transition: all 0.5s ease;
    border-top: 7px solid orange;
    padding: 25px 0;
    overflow-y: auto;
    position: absolute;
    top: 57px;
    bottom: 0;
    width: 100%;
    height: 100vh;
  }
`;
const Menu = styled.ul`
  padding: 0;
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100vh;
`;
const MenuItem = styled.li`
  display: flex;
  align-items: center;
  list-style: none;
  justify-content: center;
  width: 100%;
  margin: 0.5em 0;
  &:hover {
    border: none;
  }

  & .active {
    background: white;
    color: teal;
  }
`;
const Button = styled.a`
  /* Adapt the colors based on primary prop */
  background: transparent;
  color: white;
  cursor: pointer;
  font-size: 0.75em;
  margin: 0 0.2em;
  padding: 0.5em 0.5em;
  border: 2px solid white;
  /*border: none;*/
  border-radius: 15px;
  font-weight: bold;
  transition: all 0.5s ease;
  width: 80%;
  text-align: center;

  &:hover,
  &:active {
    background-color: white;
    color: teal;
  }
`;
const MenuIcon = styled.div`
  display: none;
  @media (max-width: 760px) {
    display: flex;
    position: absolute;
    top: 0;
    right: 0;
    height: 100%;
    align-items: center;
    transform: translate(-20%, 0);
    font-size: 3rem;
    color: black;
    font-weight: bold;
    cursor: pointer;
  }
`;
const Nav = styled.nav`
  width: 100%;
  height: 80px;
  border-bottom: 7px solid orange;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1000;
`;
const Logo = styled.div`
  position: absolute;
  top: 5px;
  left: 30px;
  cursor: pointer;
  z-index: 1000;
  display: flex;
  align-items: center;

  & img {
    width: auto;
    height: 120px;

    @media (max-width: 960px) {
      height: 100px;
    }
  }

  @media (max-width: 480px) {
    left: 15px;
  }
`;
const SearchForm = styled.form`
  display: flex;
  justify-content: flex-end;
  width: 100%;
  height: 100%;
  align-items: center;
  padding-right: 30px;

  @media (max-width: 760px) {
    display: none;
  }
`;
const SearchFormMenu = styled.form`
  display: flex;
  width: 80%;
  margin: 0.5em 10%;
`;
const InputText = styled.input.attrs({
  type: "text",
})`
  width: 100%;
  max-width: 480px;
  padding: 10px;
  font-size: 16px;
  border: 3px solid orange;
  border-radius: 15px 0 0 15px;
  box-sizing: border-box;
  resize: vertical;

  &:focus {
    outline: none;
  }

  @media (max-width: 760px) {
    padding: 5px 10px;
    border: 2px solid orange;
  }
`;
const BtnSearch = styled.button`
  color: white;
  padding: 6px 10px 6px 5px;
  border-radius: 0 15px 15px 0;
  background-color: orange;
  border: none;
  font-size: 32px;
  font-weight: bold;
  display: flex;
  align-items: center;
  cursor: pointer;
  @media (max-width: 760px) {
    padding: 1px 6px;
  }
`;
