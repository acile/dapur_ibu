import styled, { keyframes } from "styled-components"

const category = [
  {
    id: 1,
    name: "resep-daging",
    title: "Olahan Daging",
  },
  {
    id: 2,
    name: "resep-sayuran",
    title: "Olahan Sayur",
  },
  {
    id: 3,
    name: "resep-ayam",
    title: "Olahan Ayam",
  },
  {
    id: 5,
    name: "resep-seafood",
    title: "Olahan Seafood",
  },
  {
    id: 4,
    name: "resep-dessert",
    title: "Aneka Dessert",
  },
  {
    id: 6,
    name: "masakan-tradisional",
    title: "Menu Tradisional",
  },
];

export default function Sidebar() {
  return (
    <>
    <Wraper>
      <SidebarTitle>KATEGORI</SidebarTitle>
      <Main>
        {category.map((list) => (
          <a key={`category-${list.id}`} href={`/list/${list.name}`}>
            <Card img={`/${list.name}.jpg`}>
              <CardText>
                <h1>{list.title}</h1>
              </CardText>
            </Card>
          </a>
        ))}
      </Main>
      </Wraper>
    </>
  );
}

const Showup = keyframes `
  from { opacity: 0; transform:scale(0); }
  to   { opacity: 1; transform:scale(1);}
`
const CardText = styled.div `
  display: flex;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items:center;

  & h1 {
    width: 100%;
    background-color: rgba(0, 0, 0, 0.5);
    color: white;
    text-align: center;
    padding: 15px 0;
  }
`
const Card = styled.div`
  width: 100%;
  height: 100px;
  cursor: pointer;
  background-image: ${(props) => `url(${props.img})`};
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  animation: ${Showup} 0.5s;
`;
const Main = styled.div `
  width: 100%;
  display: grid;
  grid-template-columns: auto;
  grid-gap: .5em;
`
const Wraper = styled.div `
    width: 100%;
    margin-top: -22px;

    @media(max-width: 760px){
    display: none;
  }
`
const SidebarTitle = styled.h1 `
    text-align: center;
    margin-top: 0;
    margin-bottom: 10px;
`