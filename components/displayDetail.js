import styled from "styled-components";
import { MdRoomService } from "react-icons/md";
import { GiCookingPot } from "react-icons/gi";

export default function DisplayList({ detailData }) {
  const ingredient = detailData.ingredient
  const steps = detailData.step
  return (
    <>
      <Wraper>
        <ListItem img={detailData.thumb}>
          <Title>{detailData.title}</Title>
          <Label>
            <LabelItem>
              <MdRoomService />
              <span>{detailData.servings}</span>
            </LabelItem>
            <LabelItem>
              <GiCookingPot />
              <span>{detailData.dificulty}</span>
            </LabelItem>
          </Label>
        </ListItem>
        <MainContent>
        <h2 style={{textDecoration: 'underline'}}>BAHAN-BAHAN</h2>
        <ul>
        {ingredient.map((item, index) => (
          <li key={index}>{item}</li>
        ))}
        </ul>
        <h2 style={{textDecoration: 'underline'}}>Cara Pembuatan</h2>
        <ol>
        {steps.map((item, index) => (
          <li key={index}>{item.substr(2)}</li>
        ))}
        </ol>
        </MainContent>
      </Wraper>
    </>
  );
}

const MainContent = styled.div `
  padding: 0 20px;
`
const Label = styled.div`
  background-color: gold;
  width: 250px;
  float: right;
  margin-top: 0;
  margin-right: -3px;
  border-radius: 15px 0 15px 15px;
  padding: 5px 10px;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  border: 3px solid gold;
`;
const LabelItem = styled.div`
  display: flex;
  align-items: center;

  & svg {
    font-size: 24px;
    font-weight: bold;
    margin-right: 4px;
  }
`;
const Title = styled.h1`
  display: flex;
  align-items: flex-start;
  height: 175px;
  width: 100%;
  padding: 0.5em;
  margin-bottom: 0;
  margin-top: 0;
  color: black;
  font-size: 1.7em;
`;
const ListItem = styled.div`
  background-color: rgba(240, 240, 240, 0.5);
  height: 200px;
  width: 100%;
  position: relative;
  cursor: pointer;
  border: 3px solid gold;
  border-radius: 17px 17px 0 17px;

  &:after {
    content: "";
    background-image: ${(props) => `url(${props.img})`};
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    position: absolute;
    z-index: -1;
    border-radius: 15px 0 15px 15px;
  }
`;
const Wraper = styled.div`
  width: 100%;
  height: 100%;
  background-color: ivory;
  border-radius: 15px;
  z-index: -1;
`;
